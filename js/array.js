/**
 * Created by Ledstar on 14.06.2017.
 */
function execute(){
    var text = $("#inputData").val();
    $.post( "/array/sort", { text: text }, function(json) {
        if(json.result==true) {
            if(json.data) {
                $("#calcResults").show();
                $("#calcResults").text("[ "+json.data+" ]");
            }
        }
    })
}

function clearResult(){
    $("#calcResults").empty();
    $("#calcResults").hide();
}
/**
 * Created by Ledstar on 09.06.2017.
 */
function execute(){
    var text = $("#inputData").val();
    $.post( "/parser/calc", { text: text }, function(json) {
        if(json.result==true) {
            var str = "";

            if (json.data.isArray>0) {
                $.each(json.data, function (key, value) {
                    str += value + "<p>";
                });

                $("#resultArea").text(str);
                console.log(str);
            } else {
                $("#resultArea").text(json.data);
            }

            if(json.calc){
                //var ul = $("<ul class='resultUL'></ul>");
                $("#calcResults").empty();
                $.each(json.calc, function (key, value) {
                    var btn_type;
                    if(value==1) {
                        btn_type = "btn-primary";
                    }

                    if((value>1)&&(value<=10)) {
                        btn_type = "btn-success";
                    }

                    if((value>10)){
                        btn_type = "btn-warning";
                    }

                    var li = $("<button type='button' class='btn "+btn_type+"'>"+key+" <span class='badge'>"+value+"</span></button>");
                    $("#calcResults").append(li);
                });

            }
            //raiseAlert(0,"Товар с идентификатором "+name+" успешно добавлен");
        } else {
            //raiseAlert(3,"Товар не удалось добавить! Код ошибки: "+json.error);
        }
    });
}

function clearResult(){
    $("#calcResults").empty();
}

/* press button to edit item */
function editItem(id){
    var url =  "/items/edit/id/"+id;
    $.getJSON(url,function(json) {
        if(json.result==true){
            $('#myModal').modal('show');
            buildEditFrom(json.data, 1);
        } else {
            raiseAlert(3,"failed to get data from "+url);
        }
    });
}
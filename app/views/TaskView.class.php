<?php

/**
 * Created by PhpStorm.
 * User: Ledstar
 * Date: 09.06.2017
 * Time: 14:49
 */
class TaskView extends SmartView
{
    public $cssExt = array(
        "//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css"
    );

    public $jsExt = array(
        "//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js",
        "/js/highlight.js"
    );

    public function __construct($tpl) {
        parent::__construct();
        $this->template = $tpl;
        $this->loadCss($this->cssExt);
        $this->loadJs($this->jsExt);
    }

    public function setPhpCodeBlock($func1, $func2){
        $this->smarty->assign("phpFunc1", $func1);
        $this->smarty->assign("phpFunc2", $func2);
    }

    public function printResult($value){
        $this->smarty->assign('result', $value);
    }

    public function setInputData($data){
        $this->smarty->assign('inputData', $data);
    }

    public function show() {
        $this->smarty->assign('name', 'Move task');
        $this->smarty->display($this->template);
    }
}
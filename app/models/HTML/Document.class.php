<?php

/**
 * Created by PhpStorm.
 * User: Ledstar
 * Date: 10.06.2017
 * Time: 13:11
 */
class Document
{
    public $elements;
    public $levels = array();
    public function __construct(){

    }

    /**
     * @param $tag
     * @param $level
     */
    public function addElement($tag, $level){
        $this->elements[$level][] = $tag;
    }

    public function findElement($level){
        foreach ($this->elements[$level] as $i=>$node) {

        }
        # echo "NAME=".$node->getName();
        return $node;
    }

    public function calcTags(){
        $calc = array();
        foreach ($this->elements as $level=>$nodes) {
            foreach ($nodes as $node) {
                $name = strtoupper ($node->getName());
                $calc[$name]++;
            }
        }
        return $calc;
    }

    public function getLevelSize($level){
        return count($this->elements[$level]);
    }

    public function __toString() {
        // TODO: Implement __toString() method.
        $str = "";
        foreach ($this->elements as $level=>$nodes) {
            $nodeList = "nodelist: ";
            foreach ($nodes as $i=>$node) {
                $nodeList.= (string)$node; #$node->getName();
                if($i!=(count($nodes)-1)){
                    $nodeList.=",";
                }
            }

            $str.= sprintf("[%d] %s\n",$level, $nodeList);
        }
        return $str;
    }


}
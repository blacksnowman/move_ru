<?php

/**
 * Created by PhpStorm.
 * User: Ledstar
 * Date: 09.06.2017
 * Time: 17:05
 */
class HtmlTag
{
    private $name;

    private $opened = true;
    private $children = array();
    private $attributes = array();
    # private $hasChildren = 0;

    public function __construct($name = null){
        if(!is_null($name)) {
           $this->name = $name;
        }
    }

    /**
     * @param $buffer
     */
    public function parseBuffer($buffer) {
        if(preg_match_all('#(\w+)=[\'\"]*([^\'\"]+)[\'\"]*#',$buffer,$out)){
            # show_arr($out);
            for($i=0; $i<count($out[0]);$i++){
                $attributeName = $out[1][$i];
                $res = str_replace(array('"', "'"), "", $out[2][$i]);
                $attributeValue = $res;
                $this->attributes[$attributeName] = $attributeValue;
                # echo "<BR>$attributeName => $attributeValue";
            }
        }

        # $tmp = explode(' ',$buffer);
        if(preg_match('#(\w+)#',$buffer,$out)){
            $this->name = $out[1];
        } else {
            $this->name = "unknown";
        }

    }

    public function getAttribute($name) {
        return $this->attributes[$name];
    }

    public function setAttribute($name, $value){
        $this->attributes[$name] = $value;
    }

    public function isOpened(){
        return $this->opened;
    }

    public function setClosed(){
        $this->opened = false;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getName(){
        return $this->name;
    }

    public function  __isset($property) {
        return isset($this->$property);
    }

    public function hasChildren(){
        if(count($this->children)>0){
            return true;
        } else {
            return false;
        }
    }

    public function __toString(){
        if(count($this->attributes)>0) {
            foreach ($this->attributes as $attr => $value) {
                $tmp[] = sprintf('%s="%s"', $attr, $value);
            }
            $attrs = implode(" ", $tmp);
            $res = sprintf("<%s %s>", $this->name, $attrs);
        } else {
            $res = sprintf("<%s>", $this->name);
        }
        return $res;
    }
}
<?PHP
class DB extends PDO
{
	public $_db;
	const DBCONFIG = DBCONFIG;
	protected $_params;
	static private $_instance = null;
	
	private function getConfig() {
		if (file_exists(self::DBCONFIG)) {
			$this->_params = parse_ini_file(self::DBCONFIG);
			return true;
		} else {
			echo "<Br>".self::DBCONFIG." doesn't exists or couldn't be found";
			return false;
		}
	}
	
	public static function getInstance() {
		if(!self::$_instance instanceOf self)
			self::$_instance = new self;
		return self::$_instance;
	}
	
	public function query_count($sql) {
		try {
			if ($res=$this->query($sql)) {
				$arr = $res->fetch(PDO::FETCH_NUM);
				return $arr[0];
			} else {
				return false;
			}
		} catch(PDOException $e) {
			echo $e->getMessage();
			return false;
		}
	}
	
	public function exec_only($sql) {
		try {
			$res=$this->exec($sql);
			return true;
		} catch(PDOException $e) {
			echo $e->getMessage();
			return false;
		}
	}
	
	public function updateTable($tableName, $updateFields, $parameters) {
		$tmp = array();
		if (count($updateFields)>0) {
			foreach($updateFields as $key=>$value) {
				$tmp[] = "`$key`='$value'";
			}
			$updateString = implode(",",$tmp);
		} else {
			return 0;
		}
		
		$tmp = array();
		if (count($parameters)>0) {
			foreach($parameters as $key=>$value) {
				$tmp[] = "`key`='$value'";
			}
			$whereString = implode(" AND ",$tmp);
		} else {
			return 0;
		}
		$query = sprintf("UPDATE $updateString WHERE $whereString");
		echo $query;
	}
	
	public function getSingleResult($sql){
		try {
			$tmp_arr = array();
			$sth = $this->prepare($sql);
			
			if($sth->execute()) {
				$tmp_arr = $sth->fetch(PDO::FETCH_ASSOC);			
				return $tmp_arr;
			} else {
				return false;
			}
		} catch(PDOException $e) {
			echo $e->getMessage();
			return false;
		}
	}
	
	public function getResults($sql) {
		try {
			$tmp_arr = array();
			$sth = $this->prepare($sql);
			
			if($sth->execute()) {
				while($arr = $sth->fetch(PDO::FETCH_ASSOC)) {
					$tmp_arr[] = $arr;
				}
				return $tmp_arr;
			} else {
				return false;
			}
		} catch(PDOException $e) {
			echo $e->getMessage();
			return false;
		}
	}
	
	public function db_to_arr($sql,$fid,$fname) {
		try {
			if ($res=$this->query($sql)) {
				$out = array();
				while($arr = $res->fetch(PDO::FETCH_ASSOC)) {
					$out[$arr[$fid]] = $arr[$fname];
				}
				return $out;
			} else {
				return false;
			}
		} catch(PDOException $e) {
			echo $e->getMessage();
			return false;
		}
	}
	
	public function __construct() {
		if ($this->getConfig()) {
			$con_str = sprintf("mysql:host=%s;dbname=%s", $this->_params['db.host'],$this->_params['db.name']);
			try {
				# $this = new PDO($con_str ,$this->_params['db.user'],$this->_params['db.pass']);
				parent::__construct($con_str ,$this->_params['db.user'],$this->_params['db.pass']);
				$this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql = "SET NAMES 'UTF8'";
				$this->exec($sql);
			} catch(PDOException $e) {
				echo $e->getMessage();
			}
		} else {
			echo "<BR>DBConfig is not found";
		}
	}
	
	public function __destruct() {
		unset($this);
	}
}	
?>
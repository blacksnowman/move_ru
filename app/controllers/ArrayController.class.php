<?php

/**
 * Created by PhpStorm.
 * User: Ledstar
 * Date: 14.06.2017
 * Time: 1:08
 */
class ArrayController extends BasicController implements IController
{
    private $view;
    public $globArr = [4, 5, 8, 9, 1, 7, 2];
    public function __construct(){
        parent::__construct();
    }

    public function indexAction(){
        $this->view = new TaskView("array.tpl");
        $this->view->createMenu($this->menu);
        $func1 = file_get_contents("data/func1.txt");
        $func2 = file_get_contents("data/func2.txt");
        $this->view->setPhpCodeBlock($func1, $func2);
        $this->view->setInputData(implode(",",$this->globArr));
        $this->view->show();
    }

    public function sortAction(){
        #4, 5, 8, 9, 1, 7, 2,10,16,3
        if(!empty($_POST['text'])){
            $_POST['text']= str_replace(" ","",$_POST['text']);
            $arr = explode(",",$_POST['text']);
            foreach($arr as $k=>$v){
                $tmp[] = (int)$v;
            }
        } else {
            $arr = $this->globArr;
        }

        array_shaker_sort($tmp);

        if(count($tmp)>0) {
            $json['result'] = true;
            $json['data'] = $tmp;
        } else {
            $json['result'] = false;
            $json['error'] = "Error: no data provided!";
        }

        $this->helper->retJSON($json);
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: Ledstar
 * Date: 09.06.2017
 * Time: 14:28
 */
# namespace HTML\HtmlTag;

class ParserController extends BasicController implements IController
{
    private $view;
    public function __construct(){
        parent::__construct();
    }

    public function indexAction(){
        # $str = "RESULT STRING!";
        $text = file_get_contents("data/test.html");
        $this->view = new TaskView("parser.tpl");
        $this->view->createMenu($this->menu);
        $this->view->setInputData($text);
        $this->view->printResult($str);
        $this->view->show();
    }

    public function calcAction(){
        $text = $_POST['text'];
        # echo $this->helper->retTextArea($text);
        $parser  = new HtmlParser();
        if(strlen($text)>0) {
            $json['result'] = true;
            $json['data'] = $parser->parseText($text);
            $json['calc'] = $parser->calcTagsNumber();
        } else {
            $json['result'] = false;
            $json['data'] = "Error: no data provided!";
        }
        # echo $this->helper->retTextArea($res,10);
        $this->helper->retJSON($json);
    }

    public function calcResult($html){
        $parser  = new HtmlParser();
        return $parser->parseText($html);
    }
}
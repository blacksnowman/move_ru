{extends file='index.tpl'}
{block name=container}
    <div class="container" id="main">
        <div class=row>
            <div class="col-xs-12">
                <div id="alert_list"></div>
            </div>
        </div>
        <div class=row>
            <div class="col-md-6">
                <h4>Исходные данные:</h4>
                <p>Имеется массив числовых значений [4, 5, 8, 9, 1, 7, 2]. В распоряжении есть функция array_swap(&$arr, $num) { … } которая меняем местами элемент на позиции $num и элемент на 0 позиции.  Т.е. при выполнении array_swap([3, 6, 2], 2) на выходе получим [2, 6, 3].
                Написать код, сортирующий массив по возрастанию, используя только функцию array_swap.
                </p><pre><code class="php">{$phpFunc1}</code></pre>
                <div class="form-group">
                    <label for="textHTML">Input data:</label>
                    <textarea id="inputData" class="form-control" name="text" rows="2">{$inputData}</textarea>
                </div>
                <button onClick="execute();">Выполнить</button>
                <button onClick="clearResult();">Очистить</button>
            </div>
            <div class="col-md-6">
                <h4>Результат:</h4>
                <div id="calcResults"></div>
                <div><p>Была написана функция аналогичная функции шейкерной сортировки (сортировки перемешивания). Находится в папке проекта <strong>app/libraries/functions.php</strong></p></div>
                <br><pre><code class="php">{$phpFunc2}</code></pre>
            </div>
        </div>
    </div>
{/block}
{extends file='index.tpl'}
{block name=container}
    <div class="container" id="main">
        <div class=row>
            <div class="col-xs-12">
                <div id="alert_list"></div>
            </div>
        </div>

        <div class=row>
            <div class="col-md-6">
                <h4>Исходные данные:</h4>
                <p>Реализовать парсер HTML тегов.
                На выходе парсер должен выдавать какие HTML теги были в переданном HTML документе и сколько раз они были использованы.
                    Использовать готовые парсеры или библиотеки запрещено!</p>
                <!--
                <div class="form-group">
                    <label for="selectSource">Тип данных</label>
                    <select class="form-control" id="selectSource">
                        <option>База данных</option>
                        <option>Ссылка</option>
                        <option>Файл</option>
                        <option>Текст</option>
                    </select>
                </div>-->
                <div class="form-group">
                    <label for="textHTML">Input data:</label>
                    <textarea id="inputData" class="form-control" name="text" rows="15">{$inputData}</textarea>
                </div>
                <button onClick="execute();">Выполнить</button>
                <button onClick="clearResult();">Очистить</button>
            </div>
            <div class="col-md-6">
                <h4>Результат:</h4>

                <div id="calcResults"></div>
                <div class="form-group">
                    <label for="textHTML">Result data:</label>
                    <textarea class="form-control" rows="15" id="resultArea"></textarea>

                </div>


            </div>
        </div>
    </div>
{/block}